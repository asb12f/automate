#! python3
# phoneAndEmail.py - finds phone numbers adn email addresses ont eh clipboard

import re, pyperclip

phoneRegex = re.compile(r'''(
	(\d{3}|\(\d{3}\))?				# area code
	(\s|-|\.)?						# separator
	(\d{3})							# first three digits
	(\s|-|\.)						# separator
	(\d{4})							# last 4 digits
	(\s*(ext|x|ext.)\s*(\d{2,5}))?	# extension
)''', re.VERBOSE)

emailRegex = re.compile(r'''(
	[a-zA-Z0-9._%+-]+				# username
	@ 								# @ symbol
	[a-zA-Z0-9.-]+					# domain name
	(\.[a-zA-z]{2,5})				# extension
)''',re.VERBOSE)

text = str(pyperclip.paste())
matches = []
phones = 0
emails = 0
for groups in phoneRegex.findall(text):
	phoneNum = '-'.join([groups[1], groups[3], groups[5]])
	if groups[8] != '':
		phoneNum += ' x' + groups[8]
	matches.append(phoneNum)
	phones += 1
for groups in emailRegex.findall(text):
	matches.append(groups[0])
	emails += 1
	
if len(matches) > 0:
	pyperclip.copy('\n'.join(matches))
	print('Copied to clipboard')
	print('\n'.join(matches))
	print('Total ' + str(phones) + ' phone numbers and ' + str(emails) + ' emails retrieved.')
else:
	print('No phone numbers or email addresses found. Remember to copy text to be searched to the clipboard FIRST!')