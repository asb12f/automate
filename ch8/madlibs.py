import os, sys, re

# Usage:	python3 madlibs.py <text file>
#			accepted parts of speech are ADJECTIVE, NOUN, PRESENT TENSE VERB, PAST TENSE VERB, PLURAL NOUN, NUMBER

madlibFile = open(sys.argv[1])
madLib = madlibFile.read()
madlibFile.close()

word = re.compile(r'ADJECTIVE|NOUN|PRESENT TENSE VERB|PAST TENSE VERB|PLURAL NOUN, NUMBER')

words = word.findall(madLib)

for i in range(len(words)):
	if words[i] == 'ADJECTIVE':
		print('Please enter an ' + str(words[i]).lower())
	else:
		print('Please enter a ' + str(words[i]).lower())

	idea = input()
	words[i] = idea

for item in words:
	madLib = word.sub(str(item), madLib, count=1)

print(madLib)