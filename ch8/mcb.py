#! python3
# mcb.py - Saves adn loads pieces of text to the clipboard.
# Usage: 	python3 mcb.py save <keyword> - saves clipboard to keyword.
# 			python3 mcb.py <keyword> - loads keyword to clipboard
#			python3 mcb.py list - Loads all keywords to clipboard
#			python3 mcb.py delete <keyword> - deletes keyword
#			python3 mcb.py clear - deletes all keywords


import shelve
import pyperclip
import sys

mcbShelf = shelve.open('mcba')

# Save clipboard content
if len(sys.argv) == 3 and sys.argv[1].lower() == 'save':
	mcbShelf[sys.argv[2]] = pyperclip.paste()
elif len(sys.argv) == 3 and sys.argv[1].lower() == 'delete':
	del mcbShelf[sys.argv[2]]
elif len(sys.argv) == 2 and sys.argv[1].lower() == 'clear':
	for key in list(mcbShelf.keys()):
		del mcbShelf[key]
elif len(sys.argv) == 2:
# List keywords and load content.
	if sys.argv[1].lower() == 'list':
		pyperclip.copy(str(list(mcbShelf.keys())))
		print(str(list(mcbShelf.keys())))
	elif sys.argv[1] in mcbShelf:
		pyperclip.copy(mcbShelf[sys.argv[1]])
		print(mcbShelf[sys.argv[1]])
	else:
		print('No saved clip found')
		pyperclip.copy('No saved clip found')
mcbShelf.close()
