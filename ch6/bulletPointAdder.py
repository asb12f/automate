#! python3
# bulletPointAdder.py - Adds Wikipedia bullter points to the start
# of each line of text on the clipboard

import pyperclip
text = pyperclip.paste()

print('Original: \n')
print(text)

lines = text.split('\n')
for i in range(len(lines)):
	lines[i] = '* ' + lines[i]
text = '\n'.join(lines)

print('New: \n')
print(text)
pyperclip.copy(text)

