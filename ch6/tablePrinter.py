import sys

tableData = [['apples', 'oranges', 'cherries', 'banana'],
			['Alice', 'Bob', 'Carol', 'David', 'Alex', 'Maruice'],
			['dogs', 'cats', 'moose', 'goose'],
			['parrots', 'chihuahuahs', 'ice cream']]

if len(sys.argv) > 1 and sys.argv[1] == 'True':
	sort = True
else:
	sort = False
			
def printTable(data, sort=False):
	colWidths = [0] * len(data)
	maxItems = 0
	table = ''
	for column in data:
		if sort == True:
			column.sort()
		if len(column) > maxItems:
			maxItems = len(column)
		for item in column:
			if colWidths[data.index(column)] < len(item):
				colWidths[data.index(column)] = len(item)
	for i in range(maxItems):
		row = ''
		for k in range(len(data)):
			try:
				row = row + ' ' + str(data[k][i]).rjust(colWidths[k])
			except:
				row = row + ' ' + ''.rjust(colWidths[k])
		table = table + row + '\n'
	print(table)
	
printTable(tableData, sort)