import random

names = ['Ed', 'Joe', 'Bob', 'Ned', 'Mike', 'Juan']

group1 = []
group2 = []

for name in names:
	group = random.randint(1,2)
	if group == 1:
		group1.append(name)
	elif group == 2:
		group2.append(name)


print('Group 1 is:')
for name in group1:
	print('  '+name)
print('Group 2 is:')
for name in group2:
	print('  '+name)