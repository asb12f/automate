def collatz(number):
	if number % 2 == 0:
		return number // 2
	elif number % 2 == 1:
		return 3 * number + 1

def sequence():
	print ('What is the starting number?')
	number = ''
	iterations = 0
	while not number:
		try:
			number = int(input())
		except ValueError:
			print('That is not a valid integer. Please select a new starting number:')
	while True:
		number = collatz(number)
		iterations = iterations + 1
		print(number)
		if number == 1:
			print('Converged after '+ str(iterations) + ' iterations!')
			break

sequence()