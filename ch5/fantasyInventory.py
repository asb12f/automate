
def displayInventory(stuff):
	# this function prints an inventory
	print('Inventory:')
	itemTotal = 0
	for k, v in stuff.items():
		print(str(v) + ' ' + k)
		itemTotal += v
	print('Total number of items:' + str(itemTotal))

def addToInventory(stuff, addedItems):
	# this function adds items from a list to the inventory, and prints the updated inventory
	for item in addedItems:
		stuff.setdefault(item, 0)
		count = inventory.get(item)
		count += 1
		stuff[item] = count	
	displayInventory(stuff)
	return stuff

inventory = {'rope': 5, 'legos': 24500, 'mice': 100}
loot = ('Gold', 'Gold', 'rope', 'mice')
	
stuff = addToInventory(inventory, loot)
